package ru.arubtsova.tm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.endpoint.Project;
import ru.arubtsova.tm.endpoint.Task;
import ru.arubtsova.tm.endpoint.User;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@XmlRootElement
@JsonRootName("domain")
@XmlAccessorType(XmlAccessType.FIELD)
public class Domain implements Serializable {

    private String date = new Date().toString();

    @Nullable
    @JsonProperty("project")
    @JacksonXmlElementWrapper(localName = "projects")
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    private List<Project> projects;

    @Nullable
    @JsonProperty("task")
    @JacksonXmlElementWrapper(localName = "tasks")
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    private List<Task> tasks;

    @Nullable
    @JsonProperty("user")
    @JacksonXmlElementWrapper(localName = "users")
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    private List<User> users;

}
