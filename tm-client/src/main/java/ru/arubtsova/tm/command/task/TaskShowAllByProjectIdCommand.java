package ru.arubtsova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.endpoint.Session;
import ru.arubtsova.tm.endpoint.Task;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.List;

public class TaskShowAllByProjectIdCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-view-by-project-id";
    }

    @NotNull
    @Override
    public String description() {
        return "find tasks by project id.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("Tasks Overview:");
        System.out.println("Enter Project Id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final List<Task> tasks = endpointLocator.getProjectTaskEndpoint().findAllTaskByProjectId(session, projectId);
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task.getName() + " - " + task.getDescription());
            index++;
        }
    }

}
