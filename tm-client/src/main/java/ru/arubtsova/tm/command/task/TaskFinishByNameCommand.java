package ru.arubtsova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.endpoint.Session;
import ru.arubtsova.tm.endpoint.Task;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskFinishByNameCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "change task status to Complete by task name.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("Task:");
        System.out.println("Enter Task Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        final Task task = endpointLocator.getTaskEndpoint().finishByName(session, name);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

}
