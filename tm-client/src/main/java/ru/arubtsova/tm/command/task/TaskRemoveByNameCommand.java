package ru.arubtsova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.endpoint.Session;
import ru.arubtsova.tm.endpoint.Task;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @Nullable
    public String arg() {
        return null;
    }

    @NotNull
    public String name() {
        return "task-remove-by-name";
    }

    @NotNull
    public String description() {
        return "delete a task by name.";
    }

    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("Task Removal:");
        System.out.println("Enter Task Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        @Nullable final Task task = endpointLocator.getTaskEndpoint().removeByName(session, name);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("Task was successfully removed");
    }

}
