package ru.arubtsova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.endpoint.Role;
import ru.arubtsova.tm.endpoint.Session;
import ru.arubtsova.tm.util.TerminalUtil;

public class UserUnlockByLoginCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "unlock user by login.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("Unlock User:");
        System.out.println("Enter Login:");
        @NotNull final String login = TerminalUtil.nextLine();
        endpointLocator.getAdminEndpoint().unlockUserByLogin(session, login);
        System.out.println("User " + login + " was unlocked");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
