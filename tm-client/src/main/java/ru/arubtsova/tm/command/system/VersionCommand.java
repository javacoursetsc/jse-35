package ru.arubtsova.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "show application version.";
    }

    @Override
    public void execute() {
        System.out.println(Manifests.read("build"));
        //System.out.println("From property: " + endpointLocator.getPropertyService().getApplicationVersion());
    }

}