package ru.arubtsova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractProjectCommand;
import ru.arubtsova.tm.endpoint.Session;
import ru.arubtsova.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-create";
    }

    @NotNull
    @Override
    public String description() {
        return "create new project.";
    }

    @Override
    public void execute() {
        /*@NotNull final Session session = endpointLocator.getSession();
        System.out.println("Project Create:");
        System.out.println("Enter Project Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter Project Description:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Project project = endpointLocator.getProjectEndpoint().add(session, name, description);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
         */
        System.out.println("Project Create:");
        System.out.println("Enter Project Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter Project Description:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Session session = endpointLocator.getSession();
        endpointLocator.getProjectEndpoint().addProjectWithDescription(session, name, description);
    }

}
