package ru.arubtsova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractProjectCommand;
import ru.arubtsova.tm.endpoint.Project;
import ru.arubtsova.tm.endpoint.Session;
import ru.arubtsova.tm.exception.entity.ProjectNotFoundException;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectRemoveByIdWithTasksCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-id-with-tasks";
    }

    @NotNull
    @Override
    public String description() {
        return "delete project with all its tasks.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("Project Removal:");
        System.out.println("Enter Project Id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final Project project = endpointLocator.getProjectTaskEndpoint().removeProjectWithTasksById(session, projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Project was successfully removed");
    }

}
