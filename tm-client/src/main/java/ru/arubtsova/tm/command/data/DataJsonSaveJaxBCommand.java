package ru.arubtsova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractDataCommand;
import ru.arubtsova.tm.endpoint.Session;

public class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-save-jaxb";
    }

    @NotNull
    @Override
    public String description() {
        return "save json data to file by JaxB library.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("Data json save by JaxB:");
        @NotNull final Session session = endpointLocator.getSession();
        endpointLocator.getAdminEndpoint().saveJsonJaxb(session);
        System.out.println("Successful");
    }


}
