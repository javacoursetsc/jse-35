package ru.arubtsova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractDataCommand;
import ru.arubtsova.tm.endpoint.Session;

public class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-yaml-save-fasterxml";
    }

    @NotNull
    @Override
    public String description() {
        return "save yaml data to file by FasterXml library.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("Data yaml save:");
        @NotNull final Session session = endpointLocator.getSession();
        endpointLocator.getAdminEndpoint().saveYAMLFasterXML(session);
        System.out.println("Successful");
    }

}
