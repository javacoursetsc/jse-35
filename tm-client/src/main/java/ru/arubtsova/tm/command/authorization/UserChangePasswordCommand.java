package ru.arubtsova.tm.command.authorization;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.endpoint.Session;
import ru.arubtsova.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "change-password";
    }

    @NotNull
    @Override
    public String description() {
        return "change password for current user";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("Change Password:");
        System.out.println("Enter New Password:");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        endpointLocator.getUserEndpoint().setPassword(session, newPassword);
        System.out.println("Password was successfully updated");
    }

}
