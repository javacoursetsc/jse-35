package ru.arubtsova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractDataCommand;
import ru.arubtsova.tm.endpoint.Session;

public class DataBinarySaveCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-bin-save";
    }

    @NotNull
    @Override
    public String description() {
        return "save binary data to file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("Data bin save:");
        @NotNull final Session session = endpointLocator.getSession();
        endpointLocator.getAdminEndpoint().saveBin(session);
        System.out.println("Successful");
    }

}
