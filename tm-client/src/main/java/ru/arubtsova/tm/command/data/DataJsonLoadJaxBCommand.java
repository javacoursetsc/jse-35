package ru.arubtsova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractDataCommand;
import ru.arubtsova.tm.endpoint.Session;

public class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-load-jaxb";
    }

    @NotNull
    @Override
    public String description() {
        return "load json data from file by JaxB library.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("Data json load:");
        @NotNull final Session session = endpointLocator.getSession();
        endpointLocator.getAdminEndpoint().loadJsonJaxb(session);
        System.out.println("Successful");
    }

}
