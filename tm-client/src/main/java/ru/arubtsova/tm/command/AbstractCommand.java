package ru.arubtsova.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.service.EndpointLocator;
import ru.arubtsova.tm.endpoint.Role;

@NoArgsConstructor
public abstract class AbstractCommand {

    @Nullable
    protected EndpointLocator endpointLocator;

    public void setServiceLocator(@Nullable EndpointLocator endpointLocator) {
        this.endpointLocator = endpointLocator;
    }

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @Nullable
    public Role[] roles() {
        return null;
    }

}
