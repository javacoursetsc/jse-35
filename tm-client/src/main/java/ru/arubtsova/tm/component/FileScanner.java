package ru.arubtsova.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.api.service.IPropertyService;
import ru.arubtsova.tm.bootstrap.Bootstrap;
import ru.arubtsova.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FileScanner implements Runnable {

    @NotNull
    private static final String FILE_PATH = "./";

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private List<String> commands = new ArrayList<>();

    public FileScanner(@NotNull final Bootstrap bootstrap, @NotNull final IPropertyService propertyService) {
        this.bootstrap = bootstrap;
        this.propertyService = propertyService;
    }

    public void init() {
        for (@NotNull final AbstractCommand command : bootstrap.getCommandService().getCommandNames()) {
            commands.add(command.name());
        }
        es.scheduleWithFixedDelay(this, 0, propertyService.getScannerInterval(), TimeUnit.SECONDS);
    }

    public void run() {
        @NotNull final File file = new File(FILE_PATH);
        for (@NotNull final File item : file.listFiles()) {
            if (!item.isFile()) continue;
            @NotNull final String fileName = item.getName();
            final boolean check = commands.contains(fileName);
            if (!check) continue;
            bootstrap.parseCommand(fileName);
            item.delete();
        }
    }

}
