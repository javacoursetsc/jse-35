package ru.arubtsova.tm.exception.empty;

import ru.arubtsova.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! Empty Login...");
    }

}
