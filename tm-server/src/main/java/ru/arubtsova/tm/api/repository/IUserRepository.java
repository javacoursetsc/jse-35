package ru.arubtsova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.IRepository;
import ru.arubtsova.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User findByLogin(@NotNull String login);

    @NotNull
    User findByEmail(@NotNull String email);

    @Nullable
    User removeByLogin(@NotNull String login);

}
