package ru.arubtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperName();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getDeveloperCompany();

    @NotNull
    Integer getBackupInterval();

    @NotNull
    Integer getScannerInterval();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    String getValue(
            @Nullable String javaOpts,
            @Nullable String environment,
            @Nullable String defaultValue
    );

    @NotNull
    String getValueString(
            @Nullable String javaOpts,
            @Nullable String environment,
            @Nullable String defaultValue
    );

    @NotNull
    Integer getValueInteger(
            @Nullable String javaOpts,
            @Nullable String environment,
            @Nullable Integer defaultValue
    );

}
