package ru.arubtsova.tm.api.repository;

import ru.arubtsova.tm.api.IBusinessRepository;
import ru.arubtsova.tm.model.Project;

public interface IProjectRepository extends IBusinessRepository<Project> {

}
