package ru.arubtsova.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.model.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    void add(@NotNull E entity);

    void addAll(@NotNull List<E> entity);

    @NotNull
    E findById(@NotNull String id);

    void clear();

    @NotNull
    E removeById(@NotNull String id);

    void remove(@NotNull E entity);

    boolean contain(@NotNull final String id);

}
