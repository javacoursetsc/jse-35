package ru.arubtsova.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.dto.Domain;

public interface IDomainService {

    @NotNull
    Domain getDomain();

    void setDomain(@Nullable Domain domain);

    @SneakyThrows
    void loadBackup();

    @SneakyThrows
    void saveBackup();

    @SneakyThrows
    void loadBase64();

    @SneakyThrows
    void saveBase64();

    @SneakyThrows
    void loadBin();

    @SneakyThrows
    void saveBin();

    @SneakyThrows
    void loadJsonFasterXML();

    @SneakyThrows
    void loadJsonJaxb();

    @SneakyThrows
    void saveJsonFasterXML();

    @SneakyThrows
    void saveJsonJaxb();

    @SneakyThrows
    void loadXMLFasterXML();

    @SneakyThrows
    void loadXMLJaxb();

    @SneakyThrows
    void saveXMLFasterXML();

    @SneakyThrows
    void saveXMLJaxb();

    @SneakyThrows
    void loadYAMLFasterXML();

    @SneakyThrows
    void saveYAMLFasterXML();

}
