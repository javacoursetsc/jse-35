package ru.arubtsova.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull Status getStatus();

    void setStatus(@NotNull Status status);

}
